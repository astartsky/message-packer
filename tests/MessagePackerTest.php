<?php
namespace tests\Mixpanel;

use Astartsky\MessagePacker\MessagePacker;

class EventMessagePackerTest extends \PHPUnit_Framework_TestCase
{
    public function testMessagePackerWithArray()
    {
        $obj = new \stdClass();
        $obj->test = 1;

        $message = [
            'test' => 'val',
            'subset' => [
                'foo' => 'bar',
                'int' => 1,
                'float' => 1.2,
                'obj' => $obj
            ]
        ];

        $packer = new MessagePacker();
        $pack = $packer->pack($message);
        $restoredMessage = $packer->unpack($pack);

        $this->assertEquals($message, $restoredMessage);
    }

    public function testMessagePackerWithObject()
    {
        $message = new \stdClass();
        $message->a = 1;
        $message->b = "1234";

        $packer = new MessagePacker();
        $pack = $packer->pack($message);
        $restoredMessage = $packer->unpack($pack);

        $this->assertEquals($message, $restoredMessage);
    }

    public function testNonArrayMessagePack()
    {
        $message = "message";
        $packer = new MessagePacker();

        $this->setExpectedException('\Astartsky\MessagePacker\MessagePackerException', "Argument `message` must be an array or an object");

        $packer->pack($message);
    }

    public function testNonStringMessageUnpack()
    {
        $message = [];
        $packer = new MessagePacker();

        $this->setExpectedException('\Astartsky\MessagePacker\MessagePackerException', "Argument `message` must be a string");

        $packer->unpack($message);
    }
}