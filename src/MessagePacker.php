<?php
namespace Astartsky\MessagePacker;

class MessagePacker implements MessagePackerInterface
{
    /**
     * @param array $message
     * @return string
     * @throws MessagePackerException
     */
    public function pack($message)
    {
        if (is_array($message) === false && false === is_object($message)) {
            throw new MessagePackerException("Argument `message` must be an array or an object");
        }

        return base64_encode(gzcompress(serialize($message)));
    }

    /**
     * @param string $message
     * @return array
     * @throws MessagePackerException
     */
    public function unpack($message)
    {
        if (is_string($message) === false) {
            throw new MessagePackerException("Argument `message` must be a string");
        }

        return unserialize(gzuncompress(base64_decode($message)));
    }
}