<?php
namespace Astartsky\MessagePacker;

interface MessagePackerInterface
{
    /**
     * @param array $message
     * @return string
     * @throws MessagePackerException
     */
    public function pack($message);

    /**
     * @param string $message
     * @return array
     * @throws MessagePackerException
     */
    public function unpack($message);
}